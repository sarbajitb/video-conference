var response = {
    success: true,
    msg: '',
    data: undefined
};

exports.build = function (success,msg='',data=undefined) {
    if (success == true) {
        response.success = true;
        
        if (msg != '') {
            response.msg = msg;
        } else {
            response.msg = 'Operation successful!';
        }    

        if (data) {
            response.data = data;
        }
        else {
            response.data = undefined;
        }
    } else {
        response.success = false;
        if (msg != '') {
            response.msg = msg;
        } else {
            response.msg = 'Operation failed!';
        }
        response.data = undefined;
    }

    return response;
}