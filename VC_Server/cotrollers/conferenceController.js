var tokenGenerator = require('./createToken');
var rb = require('./response');
var model = require('../db/models');
var vcxroom = require('./enx/vcxroom');

const vidyoUrl = 'prod.vidyo.io';
const vidyoAppId = process.env.VIDYO_APP_ID;
const vidyoKey = process.env.VIDYO_KEY;

exports.createToken = function (req, res, next) {
    var roomId = req.body.roomId;
    var name = req.body.roomId;
    var role = req.body.role || 'participant';
    var user_ref = trim(name);
    var response;
    try {

        
        response = rb.build(true, '', token);
    } catch (error) {
        console.log('error generating vidyoio token', error);
        response = rb.build(false);
    }

    res.send(response);
};

exports.createRoom = function (req, res, next) {
    var response;
    try {
        vcxroom.createRoom(function (status, data) {
            if (status == 'error') {
                console.log('enx api call failed');
                response = rb.build(false);
                res.send(response);
                
                
            }
            else {
                console.log(data);
                response = rb.build(true);
                res.send(response);
            }
            
            
        });
    } catch (error) {
        console.log('error creating room', error);
        response = rb.build(false);
        res.send(response);
    }
    
    

        /* var room = new model.Conf({
            vidyo_appid: vidyoAppId
        });
        room.save();
        var roomId = room.room_ref;
        response = rb.build(true, 'New conference created', {
            conferenceId: roomId,
            joinLink: '/conference/join/' + roomId
        }); */
    
    
};