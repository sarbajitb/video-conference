///////////////////////////////////////////////////////
//
// Application: Multi-Party RTC: Sample Web App
// Version: 1.0.0
// The Sample Web App demonstrates the use of EnableX Server API & Web Toolkit
// to develop a Multi-Party RTC Application. The main motivation behind this
// application is to demonstrate usage of APIs and allow developers to ramp up
// on app by hosting on their own devices instead of directly using servers.
//
// Released: Nov 26, 2018
//
// File: config.js
// Service Configuration File, need to be modified as needed.
//
/////////////////////////////////////////////////////


var vcxconfig={};


// Enbalex Server API Infomration

vcxconfig.SERVER_API_SERVER={
    host: 'api.enablex.io',                    // FQDN of Service
    port: '443',                                       // PORT of Service (If specified by EnableX)
};

vcxconfig.APP_ID = process.env.APP_ID; // APP ID to access Server API
vcxconfig.APP_KEY = process.env.APP_KEY; // APP KEY to access Server API


var module = module || {};
module.exports = vcxconfig;
