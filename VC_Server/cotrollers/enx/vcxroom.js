///////////////////////////////////////////////////////
//
// File: vcxroom.js
// This file does RestAPI Call to communicate with EnableX Server API
//
// Last Updated: 29-11-2018
// Reformat, Indentation, Inline Comments
//
/////////////////////////////////////////////////////

var http = require('http')
var fs = require('fs')
var vcxconfig = require('./vcxconfig')
var vcxutil = require('./vcxutil')

// var log = require('../util/logger/logger').logger;
// var logger = log.getLogger('AppApi');
var vcxroom = {};
var options = {};


// Function: To get Token for a Room

vcxroom.getToken = function (details, callback) {
    var rooms = {};
    options.path = '/v1/rooms/' + details.roomId + '/tokens';
    options.method = 'POST';
    
    vcxutil.connectServer(options, JSON.stringify(details), function (status,data) {
        if (status === 'success')
            callback(status, data);
        else if (status === 'error')
            callback(status, data);
    });

}


// Function: To get a list of Rooms

vcxroom.getAllRooms = function (callback) {
    var rooms = {};
    options.path = '/v1/rooms/';
    options.method = 'GET';
    vcxutil.connectServer(options, null, function (status,data) {
        callback(data);
    });
}


// Function: To get information of a Room

vcxroom.getRoom = function (roomName, callback) {
    var rooms = {};
    options.path = '/v1/rooms/' + roomName;
    options.method = 'GET';
    vcxutil.connectServer(options, null, function (status,data) {
        if (status === 'success')
            callback(status, data);
        else if (status === 'error')
            callback(status, data);

    });

}

vcxroom.createRoom = function (callback) {
    var roomMeta = {
        "name": "",
        "owner_ref": "",
        "settings": {
            "scheduled": false,
            "participants": "15",
            "auto_recording": false,
            "mode": "group",
            "quality": "HD",
            "adhoc": false,
            "max_active_talkers": 4
        },
        "sip": {
            "enabled": false
        }
    };

    options.path = '/v1/rooms/';
    options.method = 'POST';
    
    vcxutil.connectServer(options, JSON.stringify(roomMeta), function (status, data) {
        if (status === 'success')
            callback(status, data);
        else if (status === 'error')
            callback(status, data);
    });
};


var module = module || {};
module.exports = vcxroom;