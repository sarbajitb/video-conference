var socketio = require('socket.io');
var redis = require('socket.io-redis');
var sio;

/**************** Event List **********************/
const EVENT_CONNECTION = 'connection';
const EVENT_USER_CREATION = 'new user';
const EVENT_DISCONNECTION = 'disconnect';
const EVENT_COMMUNICATION = 'communication';
const EVENT_SERVER_COMM = 'server-comm';
const EVENT_PRIVATE_CHAT = 'private-chat';
const EVENT_GROUP_CHAT = 'group-chat';
const EVENT_EMPLOYEE_CALL = 'employee-call';
const EVENT_BROADCAST = 'broadcast';
const EVENT_JOIN = 'join-room';
const EVENT_LEAVE = 'leave-room';
const EVENT_SHARE = 'share-file';
const EVENT_SCHEDULE = 'schedule-meeting';
/******************************************************/

exports.init = function (server) {
    try {
        sio = socketio(server);
        sio.adapter(redis({
            host: process.env.REDIS_HOST || 'localhost',
            port: process.env.REDIS_PORT || 6379
        }));
        listener(sio);
        return sio;
    } catch (error) {
        console.error("Error starting socketio.", error);
        process.exit(1);
    }
    
};

exports.io = function () {
    return sio;
};

function listener(io) {
    io.on(EVENT_CONNECTION, function (socket) {
        console.log('socket connected', socket.id);
        var roomId = socket.handshake.query.r;
        if (roomId) {
            joinRoom(roomId, socket);
        }
        socket.on(EVENT_DISCONNECTION, function () {
            console.log('socket disconnected',socket.id);
        });
    });
}

function joinRoom(roomId,socket) {
    socket.join(roomId);
    console.log('socket',socket.id,'entered room',roomId);
}
function leaveRoom(roomId, socket) {
    socket.leave(roomId);
    console.log('socket', socket.id, 'left room', roomId);
}
