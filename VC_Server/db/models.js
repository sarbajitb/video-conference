var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var Model = mongoose.model;



var confSchema = new Schema({
    room_ref: { type: String, default: shortid.generate },
    created_at: { type: Date, default: Date.now },
    started_at: { type: Date },
    ended_at: { type: Date },
    participants: [{ name: String, socketid: String, host: Boolean }],
    max_participants: Number
});

exports.Conf = Model('conferences', confSchema);


var enxSchema = new Schema({
    room_ref: { type: String },
    room_id: { type: String }
});

exports.EnxRef = Model('enxref', enxSchema);