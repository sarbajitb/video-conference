var express = require('express');
var router = express.Router();


var confCtrl = require('../cotrollers/conferenceController');

router.post('/token', confCtrl.createToken);
router.post('/room', confCtrl.createRoom);

/* GET users listing. */
router.get('/*', function(req, res, next) {
  res.render('playstore',{title:'Video Conference'});
});



module.exports = router;
