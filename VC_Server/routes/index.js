const express = require('express');
const router = express.Router();

const indexRouter = require('./indexRouter');
const conferenceRouter = require('./conferenceRouter');

router.use('/', indexRouter);
router.use('/conference', conferenceRouter);

module.exports = router;